Matterhorn Development Guides
=============================

These guides will help you if you want to participate in Matterhorn development.


 - [Development Process](development.md)
    - [Reviewing, Merging and Declining Pull Requests](reviewing-and-merging.md)
    - [Release Manager](release-manager.md)
 - [Proposal Log](proposal-log.md)
 - [Licenses and Legal Matters](license.md)
 - [Packaging Guidelines](packaging.md)
