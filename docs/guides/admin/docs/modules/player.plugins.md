# Non-standard plugins

This page lists non-standard (3rd-party) plugins for the Matterhorn Engage Player in Matterhorn version 2.x (Theodul Pass Player).

To add one of the plugins to an existing installation, the normal approach is the following:

 - Download the jar file of the plugin
 - Copy it into the Matterhorn "/lib" folder

|Name|Description|Author|License|Download|
|----|-----------|------|-------|--------|
|Download	|Lists the available the video and audio sources in a tab.	|Henning Strüber, Denis Meyer	|GNU LGPL v2 or v3	|https://bitbucket.org/CallToPower/theodul-download-plugin|
|Snow Showcase	|Let it snow in the player!	|Denis Meyer	|GNU LGPL v2 or v3	|https://bitbucket.org/CallToPower/theodul-snowshowcase-plugin|
